<style>
    .s0 {
        --sl: #00e5f9, #0578f7
    }

    .s1 {
        --sl: #fd64a5, #fe027b
    }

    .s2 {
        --sl: #c7e64b, #6fa301
    }

    .s3 {
        --sl: #f9c609, #fd7000
    }

    .s4 {
        --sl: #ce4aff, #690bf9
    }

    .s5 {
        --sl: #039aff, #042ff8
    }

    .s6 {
        --sl: #fffc3b, #fbd41d
    }

    .s7 {
        --sl: #ff867f, #fa0a15
    }
</style>
 <div class="loader-container" style="z-index: 3002;position:absolute;top: -6vh;left: 50vh;">
<div class="🍩">
    <div class="⚪ s5" style="--sf: 0.14; --tf: 1.21; --xf: 0.39;"></div>
    <div class="⚪ s6" style="--sf: 0.03; --tf: 1.17; --xf: 0.65;"></div>
    <div class="⚪ s6" style="--sf: 0.21; --tf: 1.17; --xf: 0.17;"></div>
    <div class="⚪ s7" style="--sf: 0.19; --tf: 0.85; --xf: 0.04;"></div>
    <div class="⚪ s1" style="--sf: 0.1; --tf: 1.05; --xf: 0.3;"></div>
    <div class="⚪ s0" style="--sf: 0.07; --tf: 1; --xf: 0.39;"></div>
    <div class="⚪ s7" style="--sf: 0.22; --tf: 0.91; --xf: 0.15;"></div>
    <div class="⚪ s2" style="--sf: 0.26; --tf: 1.03; --xf: 0.29;"></div>
    <div class="⚪ s3" style="--sf: 0.14; --tf: 1.08; --xf: 0.27;"></div>
    <div class="⚪ s0" style="--sf: 0.11; --tf: 0.9; --xf: 0.45;"></div>
    <div class="⚪ s6" style="--sf: 0.16; --tf: 0.91; --xf: 0.48;"></div>
    <div class="⚪ s0" style="--sf: 0.11; --tf: 0.83; --xf: 0.83;"></div>
    <div class="⚪ s0" style="--sf: 0.22; --tf: 0.78; --xf: 0.02;"></div>
    <div class="⚪ s6" style="--sf: 0.09; --tf: 0.83; --xf: 0.15;"></div>
    <div class="⚪ s5" style="--sf: 0.16; --tf: 1.15; --xf: 0.67;"></div>
    <div class="⚪ s3" style="--sf: 0.15; --tf: 1.01; --xf: 0.53;"></div>
    <div class="⚪ s3" style="--sf: 0.17; --tf: 1.05; --xf: 0.43;"></div>
    <div class="⚪ s7" style="--sf: 0.03; --tf: 1.02; --xf: 0.06;"></div>
    <div class="⚪ s1" style="--sf: 0.24; --tf: 0.93; --xf: 0.17;"></div>
    <div class="⚪ s3" style="--sf: 0.24; --tf: 1.18; --xf: 0.1;"></div>
    <div class="⚪ s5" style="--sf: 0.17; --tf: 1.13; --xf: 0.95;"></div>
    <div class="⚪ s2" style="--sf: 0.09; --tf: 0.94; --xf: 0.63;"></div>
    <div class="⚪ s6" style="--sf: 0.12; --tf: 0.88; --xf: 0.8;"></div>
    <div class="⚪ s2" style="--sf: 0.08; --tf: 1.15; --xf: 0.77;"></div>
    <div class="⚪ s1" style="--sf: 0.16; --tf: 1.1; --xf: 0.24;"></div>
    <div class="⚪ s4" style="--sf: 0.22; --tf: 1.02; --xf: 0.09;"></div>
    <div class="⚪ s2" style="--sf: 0.09; --tf: 0.78; --xf: 0.77;"></div>
    <div class="⚪ s1" style="--sf: 0.23; --tf: 0.77; --xf: 0.2;"></div>
    <div class="⚪ s4" style="--sf: 0.11; --tf: 1.03; --xf: 0.2;"></div>
    <div class="⚪ s3" style="--sf: 0.2; --tf: 0.91; --xf: 0.29;"></div>
    <div class="⚪ s2" style="--sf: 0.14; --tf: 1.21; --xf: 0.48;"></div>
    <div class="⚪ s5" style="--sf: 0.05; --tf: 0.97; --xf: 0.72;"></div>
    <div class="⚪ s6" style="--sf: 0.21; --tf: 0.92; --xf: 0.11;"></div>
    <div class="⚪ s2" style="--sf: 0.02; --tf: 0.89; --xf: 0.41;"></div>
    <div class="⚪ s7" style="--sf: 0.19; --tf: 0.76; --xf: 0.31;"></div>
    <div class="⚪ s0" style="--sf: 0.07; --tf: 0.93; --xf: 0.24;"></div>
    <div class="⚪ s6" style="--sf: 0.17; --tf: 0.88; --xf: 0.7;"></div>
    <div class="⚪ s3" style="--sf: 0.13; --tf: 1.02; --xf: 0.35;"></div>
    <div class="⚪ s7" style="--sf: 0.05; --tf: 1.15; --xf: 0.99;"></div>
    <div class="⚪ s6" style="--sf: 0.1; --tf: 1.21; --xf: 0.13;"></div>
</div>
<div class="🍩">
    <div class="⚪ s5" style="--sf: 0.07; --tf: 0.99; --xf: 0.38;"></div>
    <div class="⚪ s1" style="--sf: 0.25; --tf: 0.99; --xf: 0.52;"></div>
    <div class="⚪ s5" style="--sf: 0.1; --tf: 0.78; --xf: 0.97;"></div>
    <div class="⚪ s1" style="--sf: 0.21; --tf: 0.92; --xf: 0.08;"></div>
    <div class="⚪ s0" style="--sf: 0.06; --tf: 0.77; --xf: 0.96;"></div>
    <div class="⚪ s4" style="--sf: 0.21; --tf: 1.08; --xf: 0.14;"></div>
    <div class="⚪ s1" style="--sf: 0.04; --tf: 1.2; --xf: 0.71;"></div>
    <div class="⚪ s6" style="--sf: 0.05; --tf: 1.1; --xf: 0.02;"></div>
    <div class="⚪ s7" style="--sf: 0.23; --tf: 1.04; --xf: 0.65;"></div>
    <div class="⚪ s0" style="--sf: 0.12; --tf: 0.77; --xf: 0.57;"></div>
    <div class="⚪ s7" style="--sf: 0.22; --tf: 0.87; --xf: 0.95;"></div>
    <div class="⚪ s1" style="--sf: 0.21; --tf: 1.21; --xf: 0.49;"></div>
    <div class="⚪ s6" style="--sf: 0.24; --tf: 0.92; --xf: 0.31;"></div>
    <div class="⚪ s4" style="--sf: 0.09; --tf: 1.13; --xf: 0.44;"></div>
    <div class="⚪ s2" style="--sf: 0.21; --tf: 0.87; --xf: 0.83;"></div>
    <div class="⚪ s6" style="--sf: 0.09; --tf: 0.92; --xf: 0.24;"></div>
    <div class="⚪ s3" style="--sf: 0.25; --tf: 1.08; --xf: 0.97;"></div>
    <div class="⚪ s7" style="--sf: 0.11; --tf: 0.87; --xf: 0.32;"></div>
    <div class="⚪ s3" style="--sf: 0.23; --tf: 0.88; --xf: 0.35;"></div>
    <div class="⚪ s6" style="--sf: 0.03; --tf: 0.99; --xf: 0.41;"></div>
    <div class="⚪ s5" style="--sf: 0.12; --tf: 1.08; --xf: 0.49;"></div>
    <div class="⚪ s2" style="--sf: 0.2; --tf: 0.78; --xf: 0.2;"></div>
    <div class="⚪ s3" style="--sf: 0.17; --tf: 0.78; --xf: 0.24;"></div>
    <div class="⚪ s4" style="--sf: 0.21; --tf: 1.06; --xf: 0.28;"></div>
    <div class="⚪ s4" style="--sf: 0.15; --tf: 1.19; --xf: 0.61;"></div>
    <div class="⚪ s1" style="--sf: 0.08; --tf: 1.07; --xf: 0.07;"></div>
    <div class="⚪ s4" style="--sf: 0.13; --tf: 1.17; --xf: 0.31;"></div>
    <div class="⚪ s6" style="--sf: 0.18; --tf: 0.97; --xf: 0.68;"></div>
    <div class="⚪ s1" style="--sf: 0.07; --tf: 0.79; --xf: 0.95;"></div>
    <div class="⚪ s7" style="--sf: 0.08; --tf: 0.94; --xf: 0.25;"></div>
    <div class="⚪ s4" style="--sf: 0.09; --tf: 0.88; --xf: 0.44;"></div>
    <div class="⚪ s2" style="--sf: 0.04; --tf: 0.83; --xf: 0.81;"></div>
    <div class="⚪ s5" style="--sf: 0.14; --tf: 1.12; --xf: 0.86;"></div>
    <div class="⚪ s1" style="--sf: 0.18; --tf: 0.92; --xf: 0.59;"></div>
    <div class="⚪ s0" style="--sf: 0.2; --tf: 1.12; --xf: 0.55;"></div>
    <div class="⚪ s3" style="--sf: 0.09; --tf: 0.76; --xf: 0.49;"></div>
    <div class="⚪ s3" style="--sf: 0.14; --tf: 1.1; --xf: 0.9;"></div>
    <div class="⚪ s1" style="--sf: 0.21; --tf: 1.01; --xf: 0.56;"></div>
    <div class="⚪ s3" style="--sf: 0.06; --tf: 1.16; --xf: 0.5;"></div>
    <div class="⚪ s3" style="--sf: 0.04; --tf: 1.13; --xf: 0.4;"></div>
</div>
</div>

<style>
    @charset "UTF-8";
  /*   html {
        background: #444;
    } */

    .loader-container {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: center;
        margin: 0;
        min-height: 100vh;
        filter: drop-shadow(2px 2px 5px #000);
    }

    .🍩 {
        overflow: hidden;
        position: relative;
        margin: 1em;
        width: 13em;
        height: 13em;
        border-radius: 50%;
    }

    .🍩:nth-of-type(2) {
        --m: radial-gradient(transparent 41%, red 43%);
        -webkit-mask: var(--m);
        mask: var(--m);
    }

    .⚪ {
        position: absolute;
        top: 100%;
        left: calc(var(--xf)*100%);
        padding: calc(var(--sf)*50%);
        border-radius: 50%;
        transform: translate(-50%, 0);
        background: linear-gradient(var(--sl));
        mix-blend-mode: screen;
        animation: float calc(var(--tf)*2s) ease-out calc((var(--sf) - 1)*2s) infinite;
    }

    @keyframes float {
        to {
            transform: translate(-50%, calc(-100% - 13em));
        }
    }
</style>
