@extends('layouts.app_management')
@section("content")

<div class="main" style="min-height: 100px;">

        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                {{-- <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Check Point</h3>
                        <p class="panel-subtitle">Menual And Automatic Check Point</p>
                    </div>
                    <div class="panel-body">

                    </div>
                </div> --}}
                <!-- END OVERVIEW -->
                @include('management.dashboard.loader')

                <div class="row">
                    <div class="col-md-6">
                        <!-- RECENT PURCHASES -->
                        <div class="panel">

                                <div class="panel">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Check Point</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" id="check-point-form" >
                                            <div class="input-group">
                                                <input class="form-control" autofocus  id="qr_code"placeholder="Card QR Code #" type="text">
                                                <span class="input-group-btn"> </span>
                                            </div><br>
                                            </form>
                                        </div>
                                    </div>
                        </div>
                        <!-- END RECENT PURCHASES -->
                    </div>

                    <div class="col-md-6">
                        <!-- RECENT PURCHASES -->
                        <div class="panel">

                                <div class="panel">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Camera</h3>
                                        </div>
                                        <div class="panel-body">
                                        	<div id="my_camera"></div>

                                        </div>
                                    </div>
                        </div>
                        <!-- END RECENT PURCHASES -->
                    </div>

                </div>



            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><center> Your Parking Location:- <br><span style="color: green; text-decoration: underline;font-style: oblique;  font-size: 22px;" id="parking_name"></span></center></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <h3>Scan Barcode on mobile camera to get parking location map on mobile!</h3>
 <img src='' id='mp_qr'  style=" display: block; margin: auto; " >
{{--  <iframe id="map_frame" src="" width="100%" height="50" frameborder="0" style="border:0" allowfullscreen></iframe>
 --}}
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>
      </div>
    @endsection
    @section("script")
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	<script src="https://pixlcore.com/demos/webcamjs/webcam.min.js"></script>

    <script language="JavaScript">
        setTimeout( function(){
            Webcam.set({
			width: 320,
			height: 240,
			dest_width: 640,
			dest_height: 480,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
		Webcam.attach( '#my_camera' );
        }  , 2000 );

    </script>
    <script language="JavaScript">
        function take_snapshot() {
            data_uri = false;
            // take snapshot and get image data
             Webcam.snap( function(_data_uri) {
                 data_uri = _data_uri;
            } );
            return data_uri;
        }
    </script>
    <script>
        $('.loader-container').hide()

       openalpr =  '{{ENV('openalpr')}}';
        //function checkPoint(){
        $('#check-point-form').on('submit',function (e) {
            e.preventDefault();
            $('.loader-container').show()

            var qr_code = $('#qr_code').val()
            console.log(qr_code)
            $('#qr_code').val("")
            /// capture image and store in vehicle_images folders code here
            var imgUrl  = '{{ asset('vehicle_images/vehicle1.jpg') }}';
            var plate_data ;
            var plate ;
           // let base64image = getBase64Image(imgUrl).then(function(base64image) {
                base64image = take_snapshot();
                console.log(base64image)
                $.ajax({
                    url: "https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=us&secret_key=" + openalpr,
                    type: "post",
                    data: base64image ,
                    success: function (response) {
                        try {
                            plate_data = response;
                            if(plate_data.results &&  plate_data.results[0] && plate_data.results[0].plate){
                                plate = plate_data.results[0].plate;
                                console.log(`Request to server with ${plate,qr_code}`)
                                verify_user(plate,qr_code,base64image)
                            }else{

                                swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code,base64image)
                                });
                                $('.loader-container').hide()

                            }
                            console.log(plate);
                        }
                        catch(err) {
                            console.log(err)
                            /* swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code)
                                }); */
                                $('.loader-container').hide()

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('.loader-container').hide()
                       // swal("System need some training!")
                        swal("Number plate reading failed!  Enter plate #!", {
                                    content: "input",
                                })
                                .then((value) => {
                                    verify_user(value,qr_code)
                                });
                                $('.loader-container').hide()

                    }
                });
           /*  }, function(reason) {
                console.log(reason); // Error!
            }); */


        })

        function getBase64Image(imgUrl) {
        return new Promise(
            function(resolve, reject) {

            var img = new Image();
            img.src = imgUrl;
            img.setAttribute('crossOrigin', 'anonymous');

            img.onload = function() {
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                resolve(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
            }
            img.onerror = function() {
                reject("The image could not be loaded.");
            }

            });

        }
        function verify_user(plate,qr_code,base64image=false){
            /* plate = plate.replace(/[^a-zA-Z ]/g, "");
            plate.replace(/\s/g, '') */
            $.ajax({
               type:'POST',
               url:'{{route("management.ajaxVerifyUser")}}',
               data:{
                   "_token": "{{ csrf_token() }}",
                    "plate":plate,
                    "qr_code":qr_code,
                    "base64image":base64image
                   },
               success:function(data) {
                console.log(data)
                $('.loader-container').hide()

                if(
                    data.status == "error-user" ||
                     data.status =="error-location-user-type-vehicle" ||
                     data.status =="error-balance" ||
                     data.status =="error-slots" ||
                     data.status =="error-parking" ||
                     data.status =="error-plate-null"

                ){
                    swal("Failed!", data.message, "error");
                }
                else if(data.status =="error-vehicle"){
                    swal("Number plate reading failed!  Enter plate #!", {
                        content: "input",
                    })
                    .then((value) => {
                        verify_user(value,qr_code)
                    });
                }
                else if(data.status =="success"){
                    //swal("Good job!", data.message, "success");

                    $('#parking_name').text("");
                    $('#parking_name').text(data.message);
                    //http://maps.google.com/maps?q=loc:36.26577,-92.54324
                    $("#mp_qr").attr("src","");

                    console.log(data.url);
                    //console.log(data.map);
                    $("#mp_qr").attr("src",data.url);
                    //$("#map_frame").attr("src",data.map);

                    $("#myModal").modal("toggle")


                }
                else
                    alert("some thing went wrong");
               }
            });
        }

    </script>
    @endsection




