<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$config['name']}}</title>



    <!-- Bootstrap -->
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/linearicons/style.css') }}">
	<link rel="stylesheet" href="{{ asset('klorofil/assets/vendor/chartist/css/chartist-custom.css') }}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('klorofil/assets/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ asset('klorofil/assets/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

	<!-- help menu -->
	<link rel="stylesheet" href="{{asset('css/help.css')}}">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="{{ asset('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
	@yield("css")
</head>
<body id='bootstrap-overrides'>

<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<!-- <div class="brand">
				<a href="{{ url('/') }}"><img height="40px" width="190px" src='{{$config['logo1'] or ' '}}' alt="Klorofil Logo" class="img-responsive logo"></a>
            </div> -->
            <nav class="navbar navbar-default navbar-fixed-top">
				<div class="brand">
					<a href="{{ route('management.dashboard') }}"><img src='{{$config['logo1']}}' alt="Klorofil Logo" class="img-responsive logo"></a>
				</div>
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div id="navbar-menu">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{ Auth::user()->name }}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<!-- <li><a href="/terminal/client/{{auth()->user()->id}}/edit"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li> -->
									<!-- <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
									<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li> -->
									<li>
										<a href="{{ route('management.logout') }}"
											onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
												<i class="lnr lnr-exit"></i>  Logout
										</a>

										<form id="logout-form" action="{{ route('management.logout') }}" method="POST">
													{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
							<!-- <li>
								<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
							</li> -->
						</ul>
					</div>
				</div>
			</nav>
        </nav>

        <div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
                        <br />

						{{-- <li><a href="{{route('management.dashboard')}}" class="{{ (Route::currentRouteName() == "management.dashboard") ? 'active' : ''}}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li> --}}
						<li><a href="{{route('management.checkPoint')}}" class="{{(Route::currentRouteName() == "management.checkPoint") ? 'active' : ''}}"><i class="lnr lnr-home"></i> <span>Check Point In</span></a></li>
						<li><a href="{{route('management.checkPointOut')}}" class="{{(Route::currentRouteName() == "management.checkPointOut") ? 'active' : ''}}"><i class="lnr lnr-home"></i> <span>Check Point Out</span></a></li>






					</ul>
				</nav>
			</div>
			<div class="helper-layout">
            	<button type="button" class='help' id="help-layout">&#10068</button>
        	</div>
		</div>

        @yield('content')


            @if(!Request::is('payment-2')){
              @if($msg = session("message"))
              <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
               <i class="fa fa-check-circle"></i> {{$msg}}
              </div>

              @endif
              @if($error = session("error"))
              <div class="alert alert-danger alert-dismissible" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
               <i class="fa fa-check-circle"></i> {{$error}}
              </div>
              @endif
            }
            @endif







	<script src="{{ asset('klorofil/assets/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('klorofil/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('klorofil/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('klorofil/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
	<script src="{{ asset('klorofil/assets/vendor/chartist/js/chartist.min.js') }}"></script>
	<script src="{{ asset('klorofil/assets/vendor/chartist/js/chartist-plugin-axistitle.js') }}"></script>
	<!-- <script src="{{ asset('klorofil/assets/vendor/chartist/js/chartist-plugin-tooltip.min.js') }}"></script> -->
	<script src="{{ asset('klorofil/assets/scripts/klorofil-common.js') }}"></script>
	<script src="{{ asset('gentelella/vendors/moment/min/moment.min.js') }}"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js" integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP" crossorigin="anonymous"></script>
	<script src="{{ asset('js/time-date.js') }}"></script>

    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="{{asset('js/help.js')}}"></script>

  @yield("script")
  <script>
		$.fn.dataTable.ext.errMode = 'throw';
	</script>
    <div class="clearfix"></div>
        <footer>
			<div class="container-fluid">
				<p class="copyright">© 2017  All Rights Reserved.</p>
			</div>
        </footer>
    </div>
    </div>

</body>
</html>
