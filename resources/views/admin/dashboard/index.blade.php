@extends('layouts.app_dashboard_admin')
@section('content')

<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Parking Locations.</span>
    <div class="count">{{$location}}</div>

    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Total Parking Slots.</span>
        <div class="count">{{$slots}}</div>

      </div>

      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>Charged Amount.</span>
        <div class="count">{{$price_slot}}</div>

      </div>

      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>Registered Parking User.</span>
        <div class="count">{{$users}}</div>

      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>Security User.</span>
        <div class="count">{{$management}}</div>

      </div>


  </div>
  @endsection
