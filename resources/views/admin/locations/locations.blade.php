@extends('layouts.app_dashboard_admin')

@section("css")
    <style>
        table tr td:nth-child(5) {
            text-align: right;
        }
        .paginate_button {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
              {{--   <div class=""> --}}
                   {{--  <div class="clearfix"></div> --}}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <!-- <h2>Payment Getway </h2> -->

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                <div class="x_title">
                                <h2>Locations </h2>


                                <ul class="nav navbar-right panel_toolbox">

                                    <a class="btn btn-primary" href="{{route("admin.addLocations")}}">Create More Locations</a>
                                   </ul>

                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                 <tr>
                                                    <td>Name</td>
                                                    <td>Price</td>
                                                    <td>Number of Slots</td>
                                                    <td>Payment Type</td>
                                                    <td>Vehicle Stand Type</td>
                                                    <td>User Type</td>



                                                    <td>Status</td>
                                                    <td>Action</td>


                                                    </tr>
                                                </thead>


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
               {{--  </div> --}}


@endsection

@section("script")

<script type="text/javascript">
    $(document).ready(function () {

       var data_table = $('#table').DataTable({

       processing: true,
       serverSide: true,
       "iDisplayLength": 100,
       stateSave: true,
       "order": [[ 0, "desc" ]],
       ajax: {url:'{!! route('admin.ajaxLocation') !!}',
              data: function (d) {
              d.status = $('#filter-status').val();
              d.terminal = $('#filter-terminal').val();
              d.client = $('#filter-client').val();
              d.order_id = $('#order_id').val();

       }},
       columns: [

          {

                  "mData": null,

                  name: 'name',
                  "mRender": function (data) {
                        var edit = "{{ route('admin.viewLocation',':number') }}"
                        edit = edit.replace(':number', data.id);
                          return `<a title="view" href=${edit} style='color:blue'>${data.name}</a>`;

                       }
           },
          { data: 'price', name: 'price' },
          { data: 'number_of_slots', name: 'number_of_slots' },
          {
                  data: 'is_paid',
                  name: 'is_paid',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">PAID</span>`;
                      else
                         return `<span class="label label-primary">FREE</span>`;
                          return data;
                       }
           },
          { data: 'vehicle_type_name', name: 'vehicle_type.name' },
          { data: 'user_type_name', name: 'user_type.name' },

          {
                  data: 'is_active',
                  name: 'is_active',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">Active</span>`;
                      else
                         return `<span class="label label-danger">Blocked</span>`;
                          return data;
                       }
           },
           {
                    "mData": null,
                    "bSortable": false,
                    "bSearchable": false,
                    stateSave: true,
                    "mRender": function(data) {
                        var edit = "{{ route('admin.editLocation',':number') }}"
                        edit = edit.replace(':number', data.id);

                        var block = "{{ route('admin.blockLocation',':number') }}"
                        block = block.replace(':number', data.id);

                        var map = "{{ route('admin.map',':number') }}"
                        map = map.replace(':number', data.id);
                        if(data.is_active==1)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a title="Edit" href=${map}><i class="fa fa-map"></i> Map</a></li>
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Block</a></li>
                                </ul>
                            </div>`
                        else if(data.is_active==0)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Unblock</a></li>
                                </ul>
                            </div>`

                    }
                }

           ]
       });

       $('.filter').on('change', function (e) {

           data_table.draw();
       });
       $( ".filter" ).keyup(function() {
           data_table.draw();
       });
   });
   </script>
    <style>
        .current
        {
            background-color: #DDD !important;
        }
    </style>
@endsection
