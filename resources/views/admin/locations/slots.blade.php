@extends('layouts.app_dashboard_admin')

@section("css")
    <style>
        table tr td:nth-child(5) {
            text-align: right;
        }
        .paginate_button {
            cursor: pointer;
        }

    </style>
@endsection
@section('content')
<style>
 .boxes{
            position: inline-block;

        }

    </style>
              {{--   <div class=""> --}}
                   {{--  <div class="clearfix"></div> --}}
                    <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Locations <small>{{$location->name}}</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                            <div class="active_slot" style="margin-left:2px;width: 50px;height: 50px;background-color: greenyellow;display: inline-block;border-radius: 16px;"><cener><h2>1<h2/></center></div>




                                        </div>
                                    </div>
                                </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <!-- <h2>Payment Getway </h2> -->

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                <div class="x_title">
                                <h2>Locations  :: {{$location->name}} <small> Total Slots:  {{$location->number_of_slots}} </small> </h2>


                                <ul class="nav navbar-right panel_toolbox">

                                    <a class="btn btn-primary" href="{{route("admin.addSlots",$location->id)}}">Add Slots</a>
                                   </ul>

                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                 <tr>
                                                    <td>Slot ID</td>
                                                    <td>Availibility</td>
                                                    <td>Status</td>
                                                    <td>Vehicle #</td>

                                                    <td>Action</td>



                                                    </tr>
                                                </thead>


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
               {{--  </div> --}}


@endsection

@section("script")

<script type="text/javascript">
    $(document).ready(function () {

       var data_table = $('#table').DataTable({

       processing: true,
       serverSide: true,
       "iDisplayLength": 100,
       stateSave: true,
       "order": [[ 0, "desc" ]],
       ajax: {url:'{!! route('admin.ajaxSlots') !!}',
              data: function (d) {
              d.status = $('#filter-status').val();
              d.terminal = $('#filter-terminal').val();
              d.client = $('#filter-client').val();
              d.order_id = $('#order_id').val();
              d.location_id = {{$location->id}};

       }},
       columns: [


          { data: 'slot_id', name: 'slot_id' },
          {
                  data: 'is_booked',
                  name: 'is_booked',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">BOOKED</span>`;
                      else
                         return `<span class="label label-primary">AVAILABLE</span>`;

                       }
           },
           {
                  data: 'is_block',
                  name: 'is_block',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-danger">BLOCKED</span>`;
                      else
                         return `<span class="label label-primary">Active</span>`;

                       }
           },
           { data: 'number_plate', name: 'number_plate' },

           {
                    "mData": null,
                    "bSortable": false,
                    "bSearchable": false,
                    stateSave: true,
                    "mRender": function(data) {
                       var block = "{{ route('admin.blockSlot',':number') }}"
                        block = block.replace(':number', data.id);
                        if(data.is_block==0)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Block</a></li>
                                </ul>
                            </div>`
                        else if(data.is_block==1)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${block}><i class="fa fa-pencil"></i> Unblock</a></li>
                                 </ul>
                            </div>`

                    }
                }

           ]
       });

       $('.filter').on('change', function (e) {

           data_table.draw();
       });
       $( ".filter" ).keyup(function() {
           data_table.draw();
       });
   });
   </script>
    <style>
        .current
        {
            background-color: #DDD !important;
        }
    </style>
@endsection
