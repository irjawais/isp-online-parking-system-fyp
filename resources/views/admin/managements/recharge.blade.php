@extends('layouts.app_dashboard_admin')
@section("css")
<style>
    table tr td:nth-child(5) {
        text-align: right;
    }

    .paginate_button {
        cursor: pointer;
    }
</style>
@endsection @section('content')
<div class="">
    <div class="clearfix"></div>

    <div class="row">
            @if ($errors->any())


            @foreach ($errors->all() as $error)
                    <div  class="alert alert-danger alert-dismissible fade in" role="alert">

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>  {{$error}}</strong>
                        </div>
            @endforeach

            @endif
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Recharge <small>Recharge for user</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form id="management-form"  method="POST" action="{{route("admin.rechargePost",$user->id)}}" class="form-horizontal form-label-left" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Deposit Balance <span class="required">*</span>
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="balance" required="required" name="balance" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{-- <button class="btn btn-primary" type="button">Cancel</button> --}}
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                        <div class="x_title">
                            <h2>  Current Account Balance</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br>
                            <br>
                            <div id="card">
                             Balance RS:   {{$user->balance}}
                            </div>
                        </div>
                </div>

            <div>
    </div>
</div>


@endsection
@section("script")
<script src="{{ asset('js/bootstrap-validator/validator.min.js') }}"></script>

@endsection

