@extends('layouts.app_dashboard_admin')

@section("css")
    <style>
        table tr td:nth-child(5) {
            text-align: right;
        }
        .paginate_button {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
              {{--   <div class=""> --}}
                   {{--  <div class="clearfix"></div> --}}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <!-- <h2>Payment Getway </h2> -->

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                <div class="x_title">
                                <h2>Users </h2>


                                <ul class="nav navbar-right panel_toolbox">

                                    <a class="btn btn-primary" href="{{route("admin.addUsers")}}">Add Users</a>
                                   </ul>

                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                 <tr>
                                                    <td>Type Name</td>
                                                    <td>Name</td>
                                                    <td>Email</td>
                                                    <td>Username</td>
                                                    <td>Balance</td>
                                                    <td>Password</td>
                                                    <td>Barcode ID</td>

                                                    <td>Created Date</td>
                                                    <td>Vehicles</td>

                                                    <td>Status</td>
                                                    <td>Type</td>
                                                    <td>Action</td>


                                                    </tr>
                                                </thead>


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
               {{--  </div> --}}
               <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User : <span id="user_name"><span></h4>
                    </div>
                    <div class="modal-body">
                    <p>Details</p>
                    <p id="vehicle_content"></p>
                    </div>

                </div>

                </div>
            </div>


@endsection

@section("script")

<script type="text/javascript">
    $(document).ready(function () {

       var data_table = $('#table').DataTable({

       processing: true,
       serverSide: true,
       "iDisplayLength": 100,
       stateSave: true,
       "order": [[ 0, "desc" ]],
       ajax: {url:'{!! route('admin.ajaxUsers') !!}',
              data: function (d) {
              d.status = $('#filter-status').val();
              d.terminal = $('#filter-terminal').val();
              d.client = $('#filter-client').val();
              d.order_id = $('#order_id').val();

       }},
       columns: [
        {
                  data: 'user_type_name',
                  name: 'user_type_name',
                  "mRender": function (data) {
                      return data;
                }
           },
          { data: 'name', name: 'name' },
          { data: 'email', name: 'email' },
          { data: 'username', name: 'username' },
          { data: 'balance', name: 'balance' },
          { data: 'password_string', name: 'password_string' },
          { data: 'generated_id', name: 'generated_id' },
          { data: 'created_at', name: 'created_at' },

          {
                    "mData": null,
                    "bSortable": false,
                    "bSearchable": false,
                    stateSave: true,
                  "mRender": function (data) {
                      return `<span name=${data.name} value=${data.id} class="label label-primary vehicles">view <i class="fas fa-bicycle"></i> </span>`;

                       }
           },
          {
                  data: 'is_active',
                  name: 'is_active',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">Active</span>`;
                      else
                         return `<span class="label label-warning">Blocked</span>`;
                          return data;
                       }
           },
           {
                  data: 'is_paid',
                  name: 'is_paid',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">Paid</span>`;
                      else
                         return `<span class="label label-warning">Free</span>`;
                          return data;
                       }
           },


           {
                    "mData": null,
                    "bSortable": false,
                    "bSearchable": false,
                    stateSave: true,
                    "mRender": function(data) {
                        var edit = "{{ route('admin.editManagement',':number') }}"
                        edit = edit.replace(':number', data.id);

                        var block = "{{ route('admin.blockUser',':number') }}"
                        block = block.replace(':number', data.id);
                        var recharge = "{{ route('admin.recharge',':number') }}"
                        recharge = recharge.replace(':number', data.id);
                        if(data.is_active==1)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                        <li><a title="Edit" href=${recharge}><i class="fa fa-pencil"></i> Recharge</a></li>
                                       {{-- <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li> --}}
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Block</a></li>
                                </ul>
                            </div>`
                        else if(data.is_active==0)
                            return `
                            <div class="btn-group">
                            <a  class="btn dropdown-toggle" data-toggle="dropdown" data-disabled="true" aria-expanded="true">Options <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu" style="left:unset !important;right:0px;">
                                    <li><a title="Edit" href=${recharge}><i class="fa fa-pencil"></i> Recharge</a></li>
                                        <li><a title="Edit" href=${edit}><i class="fa fa-pencil"></i> Edit</a></li>
                                        <li><a title="Edit" href=${block}><i class="fa fa-trash"></i> Unblock</a></li>
                                </ul>
                            </div>`

                    }
                }

           ]
       });

       $('.filter').on('change', function (e) {

           data_table.draw();
       });
       $( ".filter" ).keyup(function() {
           data_table.draw();
       });

   });
   $(document).on("mouseenter", ".vehicles", function() {


        var user = $(this).attr('value')
        var name = $(this).attr('name')
        //$('#user_name').empty()
        //$('#user_name').append(name)

        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url:'{!! route('admin.usersVehicles') !!}',
            data: {
                user: user
            },

            success:function(data){
                var render_data ='';
                data.forEach(function(item) {
                    render_data += `<h5>${item.parent.name} : ${item.vehicle_number}</h5>`

                });
                //$('#vehicle_content').empty()
                //$('#vehicle_content').append(render_data)
                //console.log(render_data);
                //$('#myModal').modal('show');
                Swal.fire({
                title: `<strong>User <u>${name}</u></strong>`,
                type: 'info',
                html:render_data,
                showCloseButton: true,
                focusConfirm: false,
                confirmButtonText:
                    '<i class="fa fa-car"></i> Thanks!',
                })
            }
        });
    });
   </script>
    <style>
        .current
        {
            background-color: #DDD !important;
        }
    </style>
@endsection
