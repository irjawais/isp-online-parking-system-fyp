@extends('layouts.app_dashboard_admin')
@section("css")
<style>
    table tr td:nth-child(5) {
        text-align: right;
    }

    .paginate_button {
        cursor: pointer;
    }
</style>
@endsection @section('content')
<div class="">
    <div class="clearfix"></div>

    <div class="row">
            @if ($errors->any())


            @foreach ($errors->all() as $error)
                    <div  class="alert alert-danger alert-dismissible fade in" role="alert">

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>  {{$error}}</strong>
                        </div>
            @endforeach

            @endif
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add  User <small>create new user</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form id="management-form"  method="POST" action="{{route("admin.addUserPost")}}" class="form-horizontal form-label-left" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
                        </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name"  value="{{ old('name') }}" required="required" name="name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email"  required="required" value="{{ old('email') }}" name="email" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  required="required" value="{{ old('username') }}" name="username" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" required="required" name="password" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="terminal">User Category<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="user_type_id" id="user_type_id" class="form-control" required>
                                        <option value="" disabled selected>Select</option>
                                        @foreach($userTypes as $key => $userType)

                                                <option  value="{{$userType->id}}">{{$userType->name}}</option>

                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Initial Amount In Card <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="balance" value="{{ old('balance') }}" required="balance"  name="balance" class="form-control col-md-7 col-xs-12">
                                </div>
                        </div>

                        <div class="x_title">
                            <h2>Vehicle Detail <small>click button to add vehicle</small></h2>
                            <button type="button" class="btn btn-success float-right" style="float:right;" id="vehicle_detail_btn">Create</button>
                            <div class="clearfix"></div>
                        </div>
                        <div id="vehicle_detail">
                            <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="terminal">Vehicle Types<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="vehicle[type_id][]" class="form-control" required>
                                            <option value="" disabled selected>Select</option>
                                            @foreach($vehicleTypes as $key => $vehicleType)

                                                    <option  value="{{$vehicleType->id}}">{{$vehicleType->name}}</option>

                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Vehicle Number <span class="required">*</span>
                            </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  required="required" name="vehicle[number][]" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{-- <button class="btn btn-primary" type="button">Cancel</button> --}}
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                        <div class="x_title">
                            <h2>  User  Parking Card</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br>
                            <br>
                            <div id="card">
                                 @include("admin.managements.card")
                            </div>
                        </div>
                </div>

            <div>
    </div>
</div>


@endsection
@section("script")
<script src="{{ asset('js/bootstrap-validator/validator.min.js') }}"></script>
<script>

$('#management-form').validator()
$(document).ready(function() {

    $("#name").keyup(function(){
        $('#card_name').text("")
        $('#card_name').text(this.value)
    });
    $("#user_type_id").change(function(){
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '{!! route('admin.ajaxCheckUserType') !!}',
                data: {user_type_id:valueSelected},
                success: function(data)
                {
                    if(data==1){

                    $("#balance").prop('disabled', false);
                    $("#balance").attr("required", "true");
                    $('#card').css('display','block')
                }else{
                    $("#balance").prop('disabled', true);
                    $("#balance").removeAttr("required");
                    $('#card').css('display','none')
                    }


                }

            });

    });

   var vehicle_types = @php echo json_encode(@$vehicleTypes->toArray() ); @endphp;

$('#vehicle_detail_btn').click(function(){

var fiels  = `<hr> <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="terminal">Vehicle Types<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select name="vehicle[type_id][]" class="form-control vehicle_type_id"  required="">
                                          <option value="" disabled="" selected="">Select</option>
                                           </select>
                                      <div class="help-block with-errors"></div>
                                  </div>
                              </div>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Vehicle Number <span class="required">*</span>
                          </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text"  required="required" name="vehicle[number][]" class="form-control col-md-7 col-xs-12">
                              </div>
                          </div>`;
  $('#vehicle_detail').append(fiels)

    $.each(vehicle_types, function (i, item) {
        $('.vehicle_type_id').append($('<option>', {
            value: item.id,
            text : item.name
        }));
    });

});



});
</script>
@endsection

