@extends('layouts.app_dashboard_admin')

@section("css")
    <style>
        table tr td:nth-child(5) {
            text-align: right;
        }
        .paginate_button {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
              {{--   <div class=""> --}}
                   {{--  <div class="clearfix"></div> --}}
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <!-- <h2>Payment Getway </h2> -->

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                <div class="x_title">
                                <h2>Parking Detail </h2>


                                {{-- <ul class="nav navbar-right panel_toolbox">

                                    <a class="btn btn-primary" href="{{route("admin.addManagement")}}">Add Security User</a>
                                   </ul> --}}

                    <ul class="nav navbar-right panel_toolbox">

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                 <tr>
                                                    <td>Name</td>
                                                    <td>Vehicle </td>
                                                    <td>Vehicle #</td>

                                                {{--     <td>Fee</td> --}}

                                                    <td>Parking Date/Time</td>
                                                    <td>Charged Amount</td>

                                                    <td>Status</td>


                                                    </tr>
                                                </thead>


                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
               {{--  </div> --}}


@endsection

@section("script")

<script type="text/javascript">
    $(document).ready(function () {

       var data_table = $('#table').DataTable({

       processing: true,
       serverSide: true,
       "iDisplayLength": 100,
       stateSave: true,
       "order": [[ 0, "desc" ]],
       ajax: {url:'{!! route('admin.ajaxParkingDetail') !!}',
              data: function (d) {
              d.status = $('#filter-status').val();
              d.terminal = $('#filter-terminal').val();
              d.client = $('#filter-client').val();
              d.order_id = $('#order_id').val();

       }},
       columns: [
          { data: 'name', name: 'users.name' },
          { data: 'vehicle_type_name', name: 'vehicle_type.name' },
          { data: 'vehicle_number', name: 'vehicles.vehicle_number' },
          { data: 'created_at', name: 'created_at' },
          { data: 'charged_amount', name: 'charged_amount' },

          {
                  data: 'status',
                  name: 'status',
                  "mRender": function (data) {
                      if(data== 1)
                          return `<span class="label label-success">Occopide</span>`;
                      else
                         return `<span class="label label-warning">Released</span>`;
                          return data;
                       }
           }

           ]
       });

       $('.filter').on('change', function (e) {

           data_table.draw();
       });
       $( ".filter" ).keyup(function() {
           data_table.draw();
       });
   });
   </script>
    <style>
        .current
        {
            background-color: #DDD !important;
        }
    </style>
@endsection
