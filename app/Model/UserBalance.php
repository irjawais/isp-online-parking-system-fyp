<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{


    protected $fillable = [
        'amount',  'user_id','is_deposit'
    ];
    protected $table = "user_balance";
}
