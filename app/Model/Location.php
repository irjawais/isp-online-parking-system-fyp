<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{


    protected $fillable = [
        'name',  'institute_id','admin_id','is_active','is_paid','number_of_slots','price','vehicle_type_id','user_type_id','lat','lng'
    ];

}
