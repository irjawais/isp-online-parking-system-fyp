<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Model\Institute;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);


        try{

            $siteSettings = Institute::where('id',env('INSTITUTE_ID'))->first();
            if($siteSettings && is_array($siteSettings->toArray()) && count($siteSettings->toArray())){
                $result = array();
                $result =json_decode($siteSettings->value,true);
                $result["name"]=$siteSettings->name;
                $result["location"]=$siteSettings->location;
            }else{
                $result = [
                    "name"=>"  ",
                    "url"=> " ",
                    "from_email"=>"",
                    "from_email_title"=>" ",
                    "logo1" => " ",
                    "logo2" => " ",
                    "email_send_to"=>" "
                ];
            }
        }catch(\Exception $e){
            $result = [
                "name"=>"  ",
                "url"=> " ",
                "from_email"=>"",
                "from_email_title"=>" ",
                "logo1" => " ",
                "logo2" => " ",
                "email_send_to"=>" "
            ];
        }




        view()->composer('*', function ($view) use($result) {



                $view->with('config', $result);


        });
        config(['config' => $result]);
    }
}
