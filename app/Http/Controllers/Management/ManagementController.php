<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Model\Location;
use App\Model\Slot;
use App\Model\UserBalance;
use App\Model\UserSlot;
use App\Model\UserType;
use App\Model\Vehicle;
use App\User;
use Auth;

class ManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:management');
    }

    public function checkPoint()
    {
        return view('management.dashboard.check-point');//resources/views/admin/dashboard/index.blade.php
    }
    public function ajaxVerifyUser(Request $request){
        $user = User::where('generated_id', $request->input('qr_code'))->first();
        if(! $user)
            return array('status'=>'error-user','message'=>"Invalid Card");

        if(!$request->input('plate'))
            return array('status'=>'error-plate-null','message'=>"Number Plate Not found");
        $plate = $request->input('plate');

        $plate =preg_replace("/[^A-Za-z0-9]/", "", $plate);

        $qr_code = $request->input('qr_code');
        $user = User::where('generated_id', $request->input('qr_code'))->first();
        if(! $user)
            return array('status'=>'error-user','message'=>"Invalid Card");
        $vehicles = Vehicle::where('user_id',$user->id)->where('vehicle_number', 'like', '%' . trim($plate). '%')->first();
        //dd($vehicles->toArray() );
        if(!$vehicles)
            return array('status'=>'error-vehicle','message'=>"Unable to read vehicle plate #");
        $userType = UserType::where('id',$user->user_type_id)->first();
        //dd($vehicles->vehicle_type_id,$userType->id);
        $location = Location::where('vehicle_type_id', $vehicles->vehicle_type_id)->where('user_type_id', $userType->id)->first();

        //dd($location ,$userType->toArray(),$vehicles->toArray());
        if((!$location) || (!$userType) || (!$vehicles))
            return array('status'=>'error-location-user-type-vehicle','message'=>"Vehicle Location not available.");

        if($location->is_paid==1 && $userType->is_paid==1){

            //dd(((float)$user->balance) >=((float)$location->price));
            if(((float)$user->balance) >=((float)$location->price)){
                $user->balance =  (float)$user->balance-(float)$location->price;
                $user->save();
                UserBalance::create([
                    'is_deposit'=> 0,
                    'amount'=> $location->price,
                    'user_id'=>$user->id
                ]);
            }else{
                return array('status'=>'error-balance','message'=>"Low Balance");
            }
        }else{

        }
        $empty_slot = UserSlot::where('user_id',$user->id)->where('status',1)->first();
        if(!$empty_slot){
            $empty_slot = Slot::where('location_id',$location->id)->where('is_booked',0)->first();
            if(!$empty_slot)
                return array('status'=>'error-slots','message'=>"Slots not available.");
            $empty_slot->is_booked = 1;
            $empty_slot->save();
            UserSlot::create([
                "user_id"=>$user->id,
                "slot_id"=>$empty_slot->id,
                "vehicle_id"=>$vehicles->id,
                "charged_amount"=>$location->price,
                "base64image"=>$request->input('base64image'),
                "status"=>1
            ]);
            $url = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=http://maps.google.com/maps?q=$location->lat,$location->lng&choe=UTF-8";
            $map = "http://maps.google.com/maps?q=$location->lat,$location->lng";

            return array('map'=>$map,'status'=>'success','message'=>"Parking $location->name. => Slot # $empty_slot->slot_id",'lat'=>$location->lat,'lng'=>$location->lng,'url'=> $url );
        }else{
            return array('status'=>'error-parking','message'=>"Vehicle Already parked there.");

        }
    }
    public function checkPointOut(Request $request)
    {
       return view('management.dashboard.check-point-out');
    }
    public function ajaxVerifyUserOut(Request $request)
    {
        $user = User::where('generated_id', $request->input('qr_code'))->first();
        if(! $user)
            return array('status'=>'error-user','message'=>"Invalid Card");

        $empty_slot = UserSlot::where('user_id',$user->id)->where('status',1)->first();
        if(! $empty_slot)
            return array('status'=>'error-slot','message'=>"Slot Error!");
        $empty_slot->status = 0;
        $empty_slot->save();

        $slot = Slot::where('id',$empty_slot->slot_id)->first();
        $slot->is_booked = 0;
        $slot->save();
        return array('status'=>'success','message'=>"Parking location released");


    }

}
