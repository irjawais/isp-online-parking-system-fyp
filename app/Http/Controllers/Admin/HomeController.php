<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Model\Location;
use App\Model\Management;
use App\Model\Slot;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $location = Location::where('institute_id',Auth::guard('admin')->user()->institute_id)->count();
        $slots = Slot::where('institute_id',Auth::guard('admin')->user()->institute_id)->count();

        $price_slot = Slot::join('user_slots', function ($join) {
            $join->on('slots.id','=','user_slots.slot_id');
        })->sum("user_slots.charged_amount");
        $users = User::where('institute_id',Auth::guard('admin')->user()->institute_id)->count();
        $management = Management::where('institute_id',Auth::guard('admin')->user()->institute_id)->count();
        return view('admin.dashboard.index',compact(
            'location',
            'slots',
            'price_slot',
            'users',
            'management'
        ));//resources/views/admin/dashboard/index.blade.php
    }
     /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {

        return Auth::guard('admin');
    }

}
