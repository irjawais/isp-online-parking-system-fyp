<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Model\Management;
use Illuminate\Support\Facades\Hash;
use Redirect;
use App\Model\Location;
use App\Model\VehicleType;
use App\Model\Slot;
use App\Model\UserType;

class LocationController extends Controller
{

    public function location()
    {

        return view('admin.locations.locations');
    }

    public function addLocations()
    {
        $vehicle_types = VehicleType::where('institute_id',Auth::guard('admin')->user()->institute_id)->get();
        $user_types = UserType::where('institute_id',Auth::guard('admin')->user()->institute_id)->get();
        return view('admin.locations.add-locations',compact('vehicle_types','user_types'));
    }
    public function addLocationPost(Request $request)
    {

        $validator =  Validator::make($request->all(),[
            'name' => 'required|min:3',
            'vehicle_type_id' => 'required',

        ]);

        if($request->input('is_paid')==1){
            $validator =  Validator::make($request->all(),[
                'price' => 'required',
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        Location::create([
            'name' => $request->input('name'),
            'institute_id' => Auth::guard('admin')->user()->institute_id,
            'admin_id' => Auth::guard('admin')->user()->id,
            'is_paid' => $request->input('is_paid'),
            'price' => $request->input('price'),
            'vehicle_type_id' => $request->input('vehicle_type_id'),
            'user_type_id'=>$request->input('user_type_id')

        ]);
        $request->session()->flash('message', 'Location created successfully!');
        return Redirect::back();
    }
    public function ajaxLocation(Request $request)
    {
        $location=Location::select('locations.*','vehicle_type.name as vehicle_type_name','user_type.name as user_type_name')
            ->where('locations.institute_id',Auth::guard('admin')->user()->institute_id)
            ->join("vehicle_type","vehicle_type.id","locations.vehicle_type_id")
            ->join("user_type","user_type.id","locations.user_type_id");

        return DataTables::of($location)->make(true);
    }
    public function editLocation($id)
    {
        $location = Location::find($id);
        $vehicle_types = VehicleType::where('institute_id',Auth::guard('admin')->user()->institute_id)->get();

        return view('admin.locations.edit-locations',compact('location','vehicle_types'));
    }
    public function editLocationPost(Request $request){

        $validator =  Validator::make($request->all(),[
            'name' => 'required|min:3',
            'vehicle_type_id' => 'required',

        ]);

        if($request->input('is_paid')==1){
            $validator =  Validator::make($request->all(),[
                'price' => 'required',
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $location = Location::find( $request->input('id'));
        $location->name = $request->input('name');
        $location->is_paid = $request->input('is_paid');
        $location->price = $request->input('price');
        $location->vehicle_type_id = $request->input('vehicle_type_id');
        $location->save();
        $request->session()->flash('message', 'Location updated successfully!');
        return Redirect::back();
    }
    public function blockLocation($id,Request $request){
        $location = Location::where('id',$id)->first();
        if($location->is_active==1){
            $location->is_active=0;
            $request->session()->flash('error', 'User : '.$location->name.' blocked!');

        }else if($location->is_active==0){
            $location->is_active=1;
            $request->session()->flash('message', 'User : '.$location->name.' unblocked!');

        }
        $location->save();

        return Redirect::back();

    }
    public function viewLocation($id)
    {
        $location = Location::find($id);
        return view('admin.locations.slots',compact('location'));
    }
    public function addSlots($id){
        $location = Location::find($id);
        return view('admin.locations.add-slots',compact('location'));
    }
    public function addSlotPost(Request $request){

        $validator =  Validator::make($request->all(),[
            'location_id' => 'required',
            'number_of_slots' => 'required',
            'starting_slots' => 'required',
            //'ending_slots' => 'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $starting_slots = $request->input('starting_slots');
        $location_id = $request->input('location_id');
        $number_of_slots = $request->input('number_of_slots');
        for($i= $starting_slots; $i<=(($starting_slots+$number_of_slots)-1); $i++){
            $slot_id=$i;
            $slot = Slot::where('slot_id',$slot_id)
            ->where('location_id',$location_id)
            ->where('institute_id',Auth::guard('admin')->user()->institute_id)
            ->first();
            if($slot){
                $request->session()->flash('error', 'Slot ID : '.$slot->slot_id.' already exist!');
                return Redirect::back();

            }
        }
        for($i= $starting_slots; $i<=(($starting_slots+$number_of_slots)-1); $i++){
            $slot_id=$i;
            $slot = Slot::create([
                'slot_id'=> $slot_id,
                'location_id'=> $location_id,
                'admin_id'=>Auth::guard('admin')->user()->id,
                'institute_id'=>Auth::guard('admin')->user()->institute_id
            ]);
        }
        $total_locations = Slot::where('location_id',$location_id)
            ->where('institute_id',Auth::guard('admin')->user()->institute_id)
            ->count();
        $loc = Location::find($location_id);
        $loc->number_of_slots = $total_locations;
        $loc->save();
        $request->session()->flash('message', 'Slots created successfully!');
        return redirect()->route('admin.viewLocation',$location_id);
    }
    public function ajaxSlots(Request $request){
       $location_id =  $request->input('location_id');
       $locations = Slot::select("slots.*","vehicles.vehicle_number as number_plate")
       ->where('location_id',$location_id)
            ->where('slots.institute_id',Auth::guard('admin')->user()->institute_id)
            ->leftJoin('user_slots', function ($join) {
                $join->on('user_slots.slot_id','=','slots.id');
                $join->where('user_slots.status','=',1);
                //$join->where('slot_vehicle.status','=',1);
                //$join->where('slots.is_occupied','=',1);
            })
            ->leftJoin('vehicles', function ($join) {
                $join->on('vehicles.id','=','user_slots.id');
            })
            ;
        return DataTables::of($locations)->make(true);

    }

    public function blockSlot($id,Request $request){
        $location = Slot::where('id',$id)->first();
        if($location->is_block==1){
            $location->is_block=0;
            $request->session()->flash('message', 'Slot No : '.$location->slot_id.' unblocked!');

        }else if($location->is_block==0){
            $location->is_block=1;
            $request->session()->flash('error', 'Slot No : '.$location->slot_id.' blocked!');

        }
        $location->save();

        return Redirect::back();

    }
    public function map($id)
    {
        $location = Location::find( $id);

        return view('admin.locations.map',compact('location'));


    }
    public function mapPost($id,Request $request)
    {
        $location = Location::find( $id);
        $location->lng = $request->input("lng");
        $location->lat = $request->input("lat");
        $location->save();
        return redirect()->back();
    }

}
