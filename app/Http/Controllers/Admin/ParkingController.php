<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Model\Management;
use App\Model\UserBalance;
use App\Model\UserSlot;
use Illuminate\Support\Facades\Hash;
use Redirect;
use App\Model\VehicleType;
use App\Model\UserType;
use App\Model\Vehicle;
use App\User;

class ParkingController extends Controller
{

    public function parkingDetail()
    {

        return view('admin.managements.parking-detail');
    }
    public function  ajaxParkingDetail(Request $request){
        $management =  UserSlot::select('users.name','vehicles.vehicle_number','vehicle_type.name as vehicle_type_name','user_slots.*')
        ->join('users', 'user_slots.user_id', '=', 'users.id')
        ->join('vehicles', 'user_slots.vehicle_id', '=', 'vehicles.id')
        ->join('vehicle_type', 'vehicle_type.id', '=', 'vehicles.vehicle_type_id')

        ;
        return DataTables::of($management)->make(true);

    }


}
