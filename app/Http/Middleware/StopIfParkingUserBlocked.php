<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StopIfParkingUserBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->is_active==0){
            Auth::logout();

            abort(404,"Your account is blocked by Admin. Please contact your provider.");
        }
        return $next($request);
    }
}
