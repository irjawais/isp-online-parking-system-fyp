(function(pm, wnd, doc, $) {


    var mode = 'popup'
    var parent = null;
    var config = {
        token: '',
        vt: false,
    }

    function setupForm() {

        var options = {
            custom: {
                ccn: function($el) {

                    if (!/^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/.test($el.val())) {
                        return "Please Enter Valid Card"
                    }
                },
                cvv: function($el) {
                    if (!/^[0-9]{3,4}$/.test($el.val())) {
                        return "Please Enter Valid Cvv";
                    }
                },
                amount: function($el) {
                    if (!/^\d+(\.\d{1,2})?$/.test($el.val())) {
                        return "Please Enter Valid Amount";
                    }
                },
            }
        };
        //$("body").on("submit", "#terminal-form", function(e) {
        $('#terminal-form').validator(options).on('submit', function(e) {
            if (e.isDefaultPrevented()) {

            } else {
                e.preventDefault();
                var data = $("#terminal-form").serializeArray();
                var formData = {};
                data.forEach(element => {
                    formData[element.name] = element.value;
                });
                $("#submit").prop('disabled', true);
                $("#submitIcon").addClass("fa fa-spinner fa-spin");
                $('#terminal-form').prepend(`<b id='text_message' style='color:green;'>This process may take a few minutes, please do not refresh or navigate away from this page while your check is being processed</b>`);


                $.ajax({
                    type: "POST",

                    headers: {
                        'AuthToken': config.token,
                        'Content-Type': 'application/json'
                    },
                    url: "/api/v1.1/payment_api/js_api",
                    data: JSON.stringify(formData),
                    processData: false,
                    success: function(msg) {
                        console.log(msg);
                        $("#submit").prop('disabled', false);
                        $("#text_message").remove()

                        $("#submitIcon").removeClass("fa fa-spinner fa-spin");
                        if (msg.status == "success") {
                            if (!config.vt) {
                                parent.emit('payment-finished', msg)
                            } else
                                bootbox.alert("Your Order Placed Successfully. Transaction id is " + msg.transaction_id);
                        } else if (msg.status == "iframe-post") {
                            let iframe = $("<iframe name='iframe-post' class='frame-post' style=' width: calc(100% + 0px);'>")
                            iframe.attr("id", "iframe-post")
                            iframe.attr("src", "/process-iframe-payment/" + msg.transaction_id + "/" + config.token)
                            let outer = $("<div>")
                            outer.css({
                                "overflow": "hidden",
                                // "margin-left": "-18px",
                                // "margin-bottom": "-40px",
                                // "height": "415px",
                            })
                            if (config.vt) {
                                outer.addClass("popover-frame");
                            } else {
                                $("#form_container").hide()
                            }
                            outer.append(iframe)
                            $("body").append(outer);
                            iframe.on('payment-finish', function(event, data) {
                                    outer.remove()
                                    if (data.status == "success") {
                                        if (!config.vt) {
                                            parent.emit('payment-finished', data)
                                        } else
                                            bootbox.alert("Your Order Placed Successfully. Transaction id is " + data.transaction_id);
                                    } else {
                                        if (!data.message) {
                                            if (!config.vt) {
                                                parent.emit('payment-finished', data)
                                            } else
                                                bootbox.alert("You Order Failed. Please try again later");
                                        } else {
                                            if (!config.vt) {
                                                parent.emit('payment-finished', data)
                                            } else
                                                bootbox.alert(" " + data.message);
                                        }

                                    }
                                })
                                /* let fields = ``;
                                for (let name in msg.content) {
                                    fields += `<input name="${name}" value="${msg.content[name]}" type="hidden" />` + "\n"
                                }
                                let form = $(`
                                     <form action="https://api.epayments.store/api/transfer/v1/payIn/sendTokenForm" method="post" target="iframe-post">
                                         ${fields}
                                     </form>
                                 `)
                                $("body").append(form)
                                iframe.on('payment-finish', async(data) => {
                                    outer.remove()
                                    if (data.status == "success")
                                        bootbox.alert("Your Order Placed Successfully. Transaction id is " + data.transaction_id);
                                    else {
                                        if (!data.message)
                                            bootbox.alert("You Order Failed. Please try again later");
                                        else
                                            bootbox.alert(" " + data.message);
                                    }
                                })
                                form.submit(); */
                        }

                        /*  else if(msg.status == "otp")
                             bootbox.alert("Your Order Placed Successfully. Transaction id is "+msg.transaction_id); */
                        else {

                            if (!msg.message) {
                                if (!config.vt) {
                                    parent.emit('payment-finished', msg)
                                } else
                                    bootbox.alert("You Order Failed. Please try again later");
                            } else {
                                if (!config.vt) {
                                    parent.emit('payment-finished', msg)
                                } else
                                    bootbox.alert(" " + msg.message);
                            }

                        }
                    },
                    error: function(error) {
                        $("#submit").prop('disabled', false);
                        $("#submitIcon").removeClass("fa fa-spinner fa-spin");
                        if (error.status == 422) {
                            var errors = error.responseJSON;
                            var msg = "<p>Please resolve these errors</p>";
                            $.each(errors, function(_error, key) {
                                try {
                                    msg += `<p>${key}: ${_error[0]}</p>`
                                } catch (e) {
                                    console.log(e);
                                }

                            });
                            if (!config.vt) {
                                parent.emit('payment-error', error.responseJSON)
                            } else
                                bootbox.dialog({
                                    message: msg,
                                });
                        } else if (error.status == 500) {
                            var errors = error.responseJSON;
                            var msg = "<p>Please resolve these errors</p>";
                            $.each(errors, function(_error, key) {
                                try {
                                    msg += `<p>${key}</p>`
                                } catch (e) {
                                    console.log(e);
                                }

                            });
                            if (!config.vt) {
                                if (error.responseJSON) {
                                    parent.emit('payment-error', error.responseJSON)
                                } else {
                                    parent.emit('payment-error', [{ msg: "Internal Server" }])
                                }

                            } else
                                bootbox.dialog({
                                    message: msg,
                                });
                        } else {

                            if (!config.vt) {
                                parent.emit('payment-error', [{ msg: "You Order Failed. Please try again later" }])
                            } else
                                bootbox.alert("You Order Failed. Please try again later");
                        }

                    }
                });

            }
        })
        $('#cancelbtn').on('click', function(e) {
            parent.emit('payment-cancel', { status: "failed" });
        })

        $("body").on("change", "#billing_country", function() {
            //console.log($('#billing_country').find(":selected").val());

            /* var _country = $(this).val();
            var _country = country_iso_2[_country]; */

            var _country = $('#billing_country').find(":selected").val()

            $("select#billing_state").html("");

            $.each(states_list, function() {

                if (this.country == _country) {
                    $("select#billing_state").append('<option value="' + this.short + '">' + this.name + '</option>');
                }
            });
            if ($("select#billing_state option").length == 0 || _country == "GB") {

                $("select#billing_state").attr("disabled", true).attr("required", false).hide();
                $("#billing_state1").attr("disabled", false).show();
                $("#billing_state1").attr("required", true);
            } else {
                $("select#billing_state").attr("disabled", false).attr("required", true).show();
                $("#billing_state1").attr("disabled", true).hide();
                $("#billing_state1").attr("required", false);
            }
        })

        $("#reset").click(function() {
            location.reload();
        });

    };
    var methods = {
        openIframe: function() {

        },
        config: function(token) {
            config.token = token.token;
            config.vt = token.vt;
            parent.emit("setup-ready")
        },
        showForm: function(form_data) {
            if (form_data && form_data.gateway_id) {
                paymentFieldUrl = paymentFieldUrl + "?gateway_id=" + form_data.gateway_id;
            }
            $.ajax({
                headers: {
                    'AuthToken': config.token,
                    'Content-Type': 'application/json'
                },
                type: 'get',
                url: paymentFieldUrl,
                processData: false,
                success: function(data) {

                    if (data.type && data.type == 'Echeck') {

                        var _html_panel = $(`
                        <div class="check-holder">
                            <div class="check-inner clearfix">
                                <div class="serial-number" id="generated_number"></div>
                                <div class="date-holder clearfix">
                                    <span>Date</span>
                                    <input type="text" class="date-field" id="current_date" value="" readonly>
                                </div>
                                <div class="address">
                                    <div class="form-row">
                                    <input type="hidden" name="last_name" value="" >
                                        <input type="text" class="input-field" name="first_name" id="first_name" placeholder="Name" value="${form_data && form_data.first_name?form_data.first_name+" "+form_data.last_name:""}"  required>
                                    </div>
                                    <div class="form-row">
                                        <input type="text" class="input-field" name="billing_address1" id="billing_address1" value="${form_data && form_data.billing_address1?form_data.billing_address1:""} ${form_data && form_data.billing_address2?form_data.billing_address2:""}" placeholder="Street" required>
                                    </div>
                                    <div class="form-row clearfix">
                                        <input type="text" class="input-field street-address city" name="billing_city"  id="city" placeholder="City" value="${form_data && form_data.billing_city?form_data.billing_city:""}" required>
                                        <input type="text" class="input-field street-address state" name="billing_state" id="state" placeholder="State" value="${form_data && form_data.billing_state?form_data.billing_state:""}" required>
                                        <input type="text" class="input-field street-address"  name="billing_zip" value="${form_data && form_data.billing_zip?form_data.billing_zip:""}"  id="zipcode" placeholder="Zip Code" required>
                                    </div>
                                </div>
                                <div class="order">
                                    <span>pay to the order of</span>
                                    <div class="clearfix">
                                        <input type="text" class="input-amount" name="order"  id="order" readonly required>
                                        <div class="amount-holder clearfix">
                                            <span>$</span>
                                            <input type="text" placeholder="" class="amount" name="amount" readonly="${form_data && form_data.amount?"true":"false"}" value="${form_data && form_data.amount?form_data.amount:""}" required>
                                        </div>

                                        <div class="input-amount dollars-row" id="inwords"></div>
\                                        <div class="dollars">Dollars</div>
                                    </div>
                                </div>
                                <div class="memo-parent clearfix">
                                    <span>Memo</span>
                                    <input type="hidden" class="input-amount" name="memo" value="${form_data && form_data.order_description?form_data.order_description:""}"   id="order" readonly required>

                                    <input type="text" class="input-field" placeholder=""   name="order_description" id="order_description"   readonly>
                                    <div class="signature">
                                        <canvas class="nopadding_lr w100" id="canvas" width="170" height="50"></canvas>
                                    </div>
                                </div>
                                <div class="bottom-row clearfix">
                                    <div class="bottom-col">
                                        <input type="text" class="input-field" placeholder="Enter Routing Number " name="routing_number" required>
                                    </div>
                                    <div class="bottom-col">
                                        <input type="text" class="input-field" name="checking_account_number" placeholder="Enter Account Number " required>
                                    </div>
                                    <div class="bottom-col">
                                        <span  id="check_number"> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `)

                    }
                    if (data.type && data.type == 'Echeck') {
                        $("#form_container").append(_html_panel)
                    }
                    if (data.fields) {
                        var tabindex = 0
                        data.fields.forEach(function(item, key) {
                            var fields = item.fields


                            var html_panel = $(`<div class="col-md-12">
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title">${item.name}</h3>
                                                        </div>
                                                        <div class="panel-body" id="panel-body-${key}">

                                                        </div>
                                                    </div>
                                                </div>`)

                            //var html_section = html_panel.find(".panel-body")
                            var counter = 0;

                            var arr = [
                                $('<div class="col-md-3 col-md-offset-1"></div>'),
                                $('<div class="col-md-3 col-md-offset-1"></div>'),
                                $('<div class="col-md-3 col-md-offset-1"></div>')
                            ]
                            if (!config.vt) {
                                var elm = $('<div class="col-md-12"></div>')
                                arr = [
                                    elm,
                                    elm,
                                    elm
                                ]
                            } else {
                                arr = [
                                    $('<div class="col-md-3 col-md-offset-1"></div>'),
                                    $('<div class="col-md-3 col-md-offset-1"></div>'),
                                    $('<div class="col-md-3 col-md-offset-1"></div>')
                                ]
                            }

                            fields.forEach(function(values) {
                                var html_section = arr[counter % 3]


                                if (form_data && form_data[values.name]) {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="${form_data[values.name]}">`)
                                } else
                                if (values.type && values.type == "hidden") {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="">`)
                                } else if (values.name == "order_description" && !config.vt) {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="${form_data[values.name]?form_data[values.name]:"External Order"}">`)

                                } else if (form_data && values.name == "referrer_id" && form_data['referrer_id']) {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="${form_data[values.name]?form_data[values.name]:""}">`)

                                } else if (form_data && values.name == "billing_address2" && form_data['billing_address1']) {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="${form_data[values.name]?form_data[values.name]:""}">`)

                                } else if (values.rules.indexOf('required') == -1 && !config.vt) {
                                    html_section.append(`<input type="hidden" name="${values.name}" value="${form_data[values.name]?form_data[values.name]:""}">`)

                                } else {



                                    if (values.name == "billing_country")

                                        html_section.append(`
                                                                    <div class="form-group">
                                                                        <label>Country</label>

                                                                        <select name="billing_country" tabindex=${tabindex+1} data-required-error="Please Select Country" id="billing_country" required  class="form-control">
                                                                            <option value="">Select Country</option>
                                                                            <option value="US" selected="">United States</option>
                                                                            <option value="GB">United Kingdom</option>
                                                                            <option value="AU">Australia</option>
                                                                            <option value="CA">Canada</option>
                                                                            <option value="IN">India</option>
                                                                            <option value="BR">Brazil</option>
                                                                            <option value="">-------------------------------</option>
                                                                            <option value="AL">Albania</option>
                                                                            <option value="DZ">Algeria</option>
                                                                            <option value="AD">Andorra</option>
                                                                            <option value="AO">Angola</option>
                                                                            <option value="AI">Anguilla</option>
                                                                            <option value="AG">Antigua and Barbuda</option>
                                                                            <option value="AR">Argentina</option>
                                                                            <option value="AM">Armenia</option>
                                                                            <option value="AW">Aruba</option>
                                                                            <option value="AT">Austria</option>
                                                                            <option value="AZ">Azerbaijan Republic</option>
                                                                            <option value="BS">Bahamas</option>
                                                                            <option value="BH">Bahrain</option>
                                                                            <option value="BB">Barbados</option>
                                                                            <option value="BE">Belgium</option>
                                                                            <option value="BZ">Belize</option>
                                                                            <option value="BJ">Benin</option>
                                                                            <option value="BM">Bermuda</option>
                                                                            <option value="BT">Bhutan</option>
                                                                            <option value="BO">Bolivia</option>
                                                                            <option value="BA">Bosnia and Herzegovina</option>
                                                                            <option value="BW">Botswana</option>
                                                                            <option value="VG">British Virgin Islands</option>
                                                                            <option value="BN">Brunei</option>
                                                                            <option value="BG">Bulgaria</option>
                                                                            <option value="BF">Burkina Faso</option>
                                                                            <option value="BI">Burundi</option>
                                                                            <option value="KH">Cambodia</option>
                                                                            <option value="CV">Cape Verde</option>
                                                                            <option value="KY">Cayman Islands</option>
                                                                            <option value="TD">Chad</option>
                                                                            <option value="CL">Chile</option>
                                                                            <option value="CN">China Worldwide</option>
                                                                            <option value="CO">Colombia</option>
                                                                            <option value="KM">Comoros</option>
                                                                            <option value="CK">Cook Islands</option>
                                                                            <option value="CR">Costa Rica</option>
                                                                            <option value="HR">Croatia</option>
                                                                            <option value="CY">Cyprus</option>
                                                                            <option value="CZ">Czech Republic</option>
                                                                            <option value="CD">Democratic Republic of the Congo</option>
                                                                            <option value="DK">Denmark</option>
                                                                            <option value="DJ">Djibouti</option>
                                                                            <option value="DM">Dominica</option>
                                                                            <option value="DO">Dominican Republic</option>
                                                                            <option value="EC">Ecuador</option>
                                                                            <option value="SV">El Salvador</option>
                                                                            <option value="ER">Eritrea</option>
                                                                            <option value="EE">Estonia</option>
                                                                            <option value="ET">Ethiopia</option>
                                                                            <option value="FK">Falkland Islands</option>
                                                                            <option value="FO">Faroe Islands</option>
                                                                            <option value="FM">Federated States of Micronesia</option>
                                                                            <option value="FJ">Fiji</option>
                                                                            <option value="FI">Finland</option>
                                                                            <option value="FR">France</option>
                                                                            <option value="GF">French Guiana</option>
                                                                            <option value="PF">French Polynesia</option>
                                                                            <option value="GA">Gabon Republic</option>
                                                                            <option value="GM">Gambia</option>
                                                                            <option value="DE">Germany</option>
                                                                            <option value="GI">Gibraltar</option>
                                                                            <option value="GR">Greece</option>
                                                                            <option value="GL">Greenland</option>
                                                                            <option value="GD">Grenada</option>
                                                                            <option value="GP">Guadeloupe</option>
                                                                            <option value="GT">Guatemala</option>
                                                                            <option value="GN">Guinea</option>
                                                                            <option value="GW">Guinea Bissau</option>
                                                                            <option value="GY">Guyana</option>
                                                                            <option value="HN">Honduras</option>
                                                                            <option value="HK">Hong Kong</option>
                                                                            <option value="HU">Hungary</option>
                                                                            <option value="IS">Iceland</option>
                                                                            <option value="ID">Indonesia</option>
                                                                            <option value="IE">Ireland</option>
                                                                            <option value="IL">Israel</option>
                                                                            <option value="IT">Italy</option>
                                                                            <option value="JM">Jamaica</option>
                                                                            <option value="JP">Japan</option>
                                                                            <option value="JO">Jordan</option>
                                                                            <option value="KZ">Kazakhstan</option>
                                                                            <option value="KE">Kenya</option>
                                                                            <option value="KI">Kiribati</option>
                                                                            <option value="KW">Kuwait</option>
                                                                            <option value="KG">Kyrgyzstan</option>
                                                                            <option value="LA">Laos</option>
                                                                            <option value="LV">Latvia</option>
                                                                            <option value="LS">Lesotho</option>
                                                                            <option value="LI">Liechtenstein</option>
                                                                            <option value="LT">Lithuania</option>
                                                                            <option value="LU">Luxembourg</option>
                                                                            <option value="MG">Madagascar</option>
                                                                            <option value="MW">Malawi</option>
                                                                            <option value="MY">Malaysia</option>
                                                                            <option value="MV">Maldives</option>
                                                                            <option value="ML">Mali</option>
                                                                            <option value="MT">Malta</option>
                                                                            <option value="MH">Marshall Islands</option>
                                                                            <option value="MQ">Martinique</option>
                                                                            <option value="MR">Mauritania</option>
                                                                            <option value="MU">Mauritius</option>
                                                                            <option value="YT">Mayotte</option>
                                                                            <option value="MX">Mexico</option>
                                                                            <option value="MN">Mongolia</option>
                                                                            <option value="MS">Montserrat</option>
                                                                            <option value="MA">Morocco</option>
                                                                            <option value="MZ">Mozambique</option>
                                                                            <option value="NA">Namibia</option>
                                                                            <option value="NR">Nauru</option>
                                                                            <option value="NP">Nepal</option>
                                                                            <option value="NL">Netherlands</option>
                                                                            <option value="AN">Netherlands Antilles</option>
                                                                            <option value="NC">New Caledonia</option>
                                                                            <option value="NZ">New Zealand</option>
                                                                            <option value="NI">Nicaragua</option>
                                                                            <option value="NE">Niger</option>
                                                                            <option value="NU">Niue</option>
                                                                            <option value="NF">Norfolk Island</option>
                                                                            <option value="NO">Norway</option>
                                                                            <option value="OM">Oman</option>
                                                                            <option value="PW">Palau</option>
                                                                            <option value="PA">Panama</option>
                                                                            <option value="PG">Papua New Guinea</option>
                                                                            <option value="PE">Peru</option>
                                                                            <option value="PH">Philippines</option>
                                                                            <option value="PN">Pitcairn Islands</option>
                                                                            <option value="PL">Poland</option>
                                                                            <option value="PT">Portugal</option>
                                                                            <option value="QA">Qatar</option>
                                                                            <option value="CD">Republic of the Congo</option>
                                                                            <option value="RE">Reunion</option>
                                                                            <option value="ROM">Romania</option>
                                                                            <option value="RU">Russia</option>
                                                                            <option value="RW">Rwanda</option>
                                                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                                                            <option value="WS">Samoa</option>
                                                                            <option value="SM">San Marino</option>
                                                                            <option value="ST">São Tomé and Príncipe</option>
                                                                            <option value="SA">Saudi Arabia</option>
                                                                            <option value="SN">Senegal</option>
                                                                            <option value="SC">Seychelles</option>
                                                                            <option value="SL">Sierra Leone</option>
                                                                            <option value="SG">Singapore</option>
                                                                            <option value="SK">Slovakia</option>
                                                                            <option value="SI">Slovenia</option>
                                                                            <option value="SB">Solomon Islands</option>
                                                                            <option value="SO">Somalia</option>
                                                                            <option value="ZA">South Africa</option>
                                                                            <option value="KR">South Korea</option>
                                                                            <option value="ES">Spain</option>
                                                                            <option value="LK">Sri Lanka</option>
                                                                            <option value="SH">St. Helena</option>
                                                                            <option value="KN">St. Kitts and Nevis</option>
                                                                            <option value="LC">St. Lucia</option>
                                                                            <option value="PM">St. Pierre and Miquelon</option>
                                                                            <option value="SR">Suriname</option>
                                                                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="SZ">Swaziland</option>
                                                                            <option value="SE">Sweden</option>
                                                                            <option value="CH">Switzerland</option>
                                                                            <option value="TW">Taiwan</option>
                                                                            <option value="TJ">Tajikistan</option>
                                                                            <option value="TZ">Tanzania</option>
                                                                            <option value="TH">Thailand</option>
                                                                            <option value="TG">Togo</option>
                                                                            <option value="TO">Tonga</option>
                                                                            <option value="TT">Trinidad and Tobago</option>
                                                                            <option value="TN">Tunisia</option>
                                                                            <option value="TR">Turkey</option>
                                                                            <option value="TM">Turkmenistan</option>
                                                                            <option value="TC">Turks and Caicos Islands</option>
                                                                            <option value="TV">Tuvalu</option>
                                                                            <option value="UG">Uganda</option>
                                                                            <option value="UA">Ukraine</option>
                                                                            <option value="AE">United Arab Emirates</option>
                                                                            <option value="UY">Uruguay</option>
                                                                            <option value="VU">Vanuatu</option>
                                                                            <option value="VA">Vatican City State</option>
                                                                            <option value="VE">Venezuela</option>
                                                                            <option value="VN">Vietnam</option>
                                                                            <option value="WF">Wallis and Futuna Islands</option>
                                                                            <option value="YE">Yemen</option>
                                                                            <option value="ZM">Zambia</option>
                                                                        </select>
                                                                        <div class="help-block with-errors"></div>
                                                                </div>


                                                    `)
                                    else if (values.name == "customer_ip") {
                                        counter--;

                                        html_section.append(`<input type="hidden" name="customer_ip" value="${customer_ip}">`)

                                    } else if (values.name == "expiry_month") {
                                        html_section.append(`
                                                                    <div class="form-group">
                                                                            <label>${values.label}</label>
                                                                            <select name="expiry_month" id="expiry_month" class="form-control" tabindex=${tabindex+1} required>
                                                                                <option value="01">January</option>
                                                                                <option value="02">February</option>
                                                                                <option value="03">March</option>
                                                                                <option value="04">April</option>
                                                                                <option value="05">May</option>
                                                                                <option value="06">June</option>
                                                                                <option value="07">July</option>
                                                                                <option value="08">August</option>
                                                                                <option value="09">September</option>
                                                                                <option value="10">October</option>
                                                                                <option value="11">November</option>
                                                                                <option value="12">December</option>
                                                                            </select>
                                                                            <div class="help-block with-errors"></div>
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>
                                                                    </div>

                                                    `)
                                    } else if (values.name == "expiry_year") {
                                        html_section.append(`

                                                                        <div class="form-group">
                                                                            <label>${values.label}</label>
                                                                            <select name="expiry_year" id="expiry_year" class="form-control" tabindex=${tabindex+1} required>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                                <option value="2031">2031</option>
                                                                                <option value="2032">2032</option>
                                                                                <option value="2033">2033</option>
                                                                                <option value="2034">2034</option>
                                                                                <option value="2035">2035</option>
                                                                                <option value="2036">2036</option>
                                                                            </select>
                                                                            <div class="help-block with-errors"></div>
                                                                            <div class="help-block with-errors"></div>
                                                                        </div>


                                                    `)
                                    } else if (values.name == "billing_state") {
                                        html_section.append(`
                                                                <div class="form-group">
                                                                        <label id="lable_state">State</label>
                                                                        <select id="billing_state" tabindex=${tabindex+1}  data-required-error="Please Select State"  name="billing_state" class="form-control">
                                                                                <option value="AL">Alabama</option>
                                                                                <option value="AK">Alaska</option>
                                                                                <option value="AS">American Samoa</option>
                                                                                <option value="AZ">Arizona</option>
                                                                                <option value="AR">Arkansas</option>
                                                                                <option value="CA">California</option>
                                                                                <option value="CO">Colorado</option>
                                                                                <option value="CT">Connecticut</option>
                                                                                <option value="DE">Delaware</option>
                                                                                <option value="DC">District of Columbia</option>
                                                                                <option value="FL">Florida</option>
                                                                                <option value="GA">Georgia</option>
                                                                                <option value="GU">Guam</option>
                                                                                <option value="HI">Hawaii</option>
                                                                                <option value="ID">Idaho</option>
                                                                                <option value="IL">Illinois</option>
                                                                                <option value="IN">Indiana</option>
                                                                                <option value="IA">Iowa</option>
                                                                                <option value="KS">Kansas</option>
                                                                                <option value="KY">Kentucky</option>
                                                                                <option value="LA">Louisiana</option>
                                                                                <option value="ME">Maine</option>
                                                                                <option value="MD">Maryland</option>
                                                                                <option value="MA">Massachusetts</option>
                                                                                <option value="MI">Michigan</option>
                                                                                <option value="MN">Minnesota</option>
                                                                                <option value="MS">Mississippi</option>
                                                                                <option value="MO">Missouri</option>
                                                                                <option value="MT">Montana</option>
                                                                                <option value="NE">Nebraska</option>
                                                                                <option value="NV">Nevada</option>
                                                                                <option value="NH">New Hampshire</option>
                                                                                <option value="NJ">New Jersey</option>
                                                                                <option value="NM">New Mexico</option>
                                                                                <option value="NY">New York</option>
                                                                                <option value="NC">North Carolina</option>
                                                                                <option value="ND">North Dakota</option>
                                                                                <option value="MP">Northern Mariana Islands</option>
                                                                                <option value="OH">Ohio</option>
                                                                                <option value="OK">Oklahoma</option>
                                                                                <option value="OR">Oregon</option>
                                                                                <option value="PA">Pennsylvania</option>
                                                                                <option value="PR">Puerto Rico</option>
                                                                                <option value="RI">Rhode Island</option>
                                                                                <option value="SC">South Carolina</option>
                                                                                <option value="SD">South Dakota</option>
                                                                                <option value="TN">Tennessee</option>
                                                                                <option value="TX">Texas</option>
                                                                                <option value="UM">United States Minor Outlying Islands</option>
                                                                                <option value="UT">Utah</option>
                                                                                <option value="VT">Vermont</option>
                                                                                <option value="VI">Virgin Islands, U.S.</option>
                                                                                <option value="VA">Virginia</option>
                                                                                <option value="WA">Washington</option>
                                                                                <option value="WV">West Virginia</option>
                                                                                <option value="WI">Wisconsin</option>
                                                                                <option value="WY">Wyoming</option>
                                                                        </select>
                                                                        <input type="text" class="form-control" style="display:none"  data-required-error="Please Enter State"  tabindex=${tabindex+1}  placeholder="State" name="billing_state" id="billing_state1"  disabled>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>


                                                                    `)

                                    } else if (values.name == "billing_address2") {
                                        html_section.append(`
                                                                 <div class="form-group">
                                                                        <label>${values.label}</label>
                                                                        <input type="text" class="form-control has-feedback-left" tabindex=${tabindex+1}  data-required-error="Please Enter ${values.label}"  name="${values.name}"  placeholder="Please Enter ${values.label}" >
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>


                                                `)
                                    } else

                                        html_section.append(`
                                                                 <div class="form-group">
                                                                        <label>${values.label}</label>
                                                                        <input type="text" class="form-control has-feedback-left" tabindex=${tabindex+1}  data-required-error="Please Enter ${values.label}"  name="${values.name}"  placeholder="Please Enter ${values.label}" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>


                                                `)
                                    counter++
                                    tabindex++
                                }

                            });
                            if (config.vt) {
                                html_panel.find(".panel-body").append(arr[0])
                                html_panel.find(".panel-body").append(arr[1])
                                html_panel.find(".panel-body").append(arr[2])
                            } else {
                                html_panel.find(".panel-body").append(arr[0])
                            }


                            if (key == data.fields.length - 1) {

                                if (!config.vt && form_data.amount) {

                                    html_panel.find(".panel-body").append(` <div class="col-md-12" id="amount-panel"><h2 class="text-right" >Amount: $${parseFloat(form_data.amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</h2></div>`);
                                    $('#reset').remove()
                                    $('#submit').text("Pay")
                                }

                                if (!data.type || data.type != 'Echeck') {
                                    if (form_data && form_data.gateway_id) {
                                        html_panel.append(`<input type="hidden" name="gateway_id" value="${form_data.gateway_id}">`)
                                    }
                                    html_panel.append(`
                                                                <div class="col-md-12">

                                                                </div>
                                                                <div class="col-md-4">

                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                            <button id="reset" class="btn btn-primary" type="reset">Reset</button>
                                                                            <button id="submit" type="submit"  class="btn btn-success" > <i id="submitIcon"> </i> Submit</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">

                                                                </div>
                                        `)
                                } else {
                                    if (form_data && form_data.gateway_id) {
                                        html_panel.append(`<input type="hidden" name="gateway_id" value="${form_data.gateway_id}">`)

                                    }
                                    html_panel.append(`


                                                                <div class="text-center">
                                                                           <a href="javascript:" id="cancelbtn" class="pull-left" style="color:grey">Cancel</a>
                                                                            <button id="submit" type="submit"  class="btn btn-success" >Approve Payment</button>

                                                                </div>

                                        `)
                                }

                            }


                            $("#form_container").append(html_panel)


                            if (!config.vt && (!data.type || data.type != 'Echeck')) {
                                $('#reset').remove()
                                $('#submit').text("Pay")
                                $('#submit').css('float', 'right')
                            }
                            if ($('.panel').eq(0).find('.form-group').length == 0) {
                                $('.panel').eq(0).hide()
                            }
                        });
                    }

                    if (data.script) {
                        // set-model-width
                        $("#form_container").on("set-model-width", function(event, data) {
                            parent.emit('set-model-width', data)
                        })

                        setTimeout(function() {
                                $("#form_container").append($(`
                            <script>
                                ${data.script}
                            </script>
                        `))
                            },
                            100)
                    }

                    setupForm()
                    if (data.type && data.type == 'Echeck') {
                        //parent.emit('keep-open-on-failed')
                    }
                    /* if(data.model_width){
                        parent.emit('set-model-width',data.model_width)
                    } */
                    parent.emit('form-loaded')
                }
            });


        },
        height: () => document.height || document.body.offsetHeight,
        openIframeModel: function() {

        },


        postFormToIframe: function(trans_id, data) {
            $.ajax({
                url: '/load-iframe/' + trans_id,
                headers: {
                    'AuthToken': `${config.token}`,
                },
                success: function(url) {
                    let iframe = $("<iframe name='iframe-post' style='width:100%;height:100%' class='post-form-iframe' sandbox='allow-same-origin allow-scripts allow-pointer-lock allow-forms allow-popups' >")
                    iframe.attr("id", "iframe-post")
                    iframe.attr("action", url)

                    let outer = $("<div>")
                    outer.append(iframe)
                    $("body").append(outer);
                    let fields = ``;
                    for (let name in data) {
                        fields += `<input name="${name}" value="${data[name]}" type="hidden" />` + "\n"
                    }
                    let form = $(`
                        <form action="${url}" method="post" target="iframe-post">
                            ${fields}
                        </form>
                    `)
                    $("body").append(form)
                    iframe.on("payment-data", function(msg) {})
                    form.submit();
                }
            })
        }

    }



    const handshake = new pm.Model(methods);

    handshake.then(_parent => {
        parent = _parent
    });

    $('#iframe-post').on("response-recived", function(data) {
        console.log(data);
    })

    wnd.addEventListener("message", function(event) {
        if (event.data && event.data.cmd == "transaction-status") {
            $('#iframe-post').trigger('payment-finish', event.data.trans)
        }
    })


})(Postmate, window, document, jQuery)