var modal = document.getElementById('help-modal');
var btn = document.getElementById('help');
var span = document.getElementById("close");

btn.onclick = function() {
    modal.style.display = 'block';
}

span.onclick = function() {
    modal.style.display = 'none';
}

window.onclick = function() {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

$('section.awSlider .carousel').carousel({
	pause: "hover",
  interval: 10000000000000000
});

var startImage = $('section.awSlider .item.active > img').attr('src');
$('section.awSlider').append('<img src="' + startImage + '">');

$('section.awSlider .carousel').on('slid.bs.carousel', function () {
 var bscn = $(this).find('.item.active > img').attr('src');
	$('section.awSlider > img').attr('src',bscn);
});
