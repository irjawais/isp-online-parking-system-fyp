<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('slot_id')->nullable();
            $table->boolean('is_booked')->default(0);
            $table->boolean('is_block')->default(0);



            $table->bigInteger('location_id')->unsigned()->index();
            $table->foreign('location_id')->references('id')->on('locations');
        });
        Schema::table('slots', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();
            $table->bigInteger('admin_id')->unsigned()->index();

            $table->foreign('institute_id')->references('id')->on('institute');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_id');
        Schema::dropIfExists('institute_id');
        Schema::dropIfExists('admin_id');

        Schema::dropIfExists('slots');
    }
}
