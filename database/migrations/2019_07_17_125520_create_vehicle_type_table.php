<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name')->nullable();

        });

        Schema::table('vehicle_type', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();
            $table->bigInteger('admin_id')->unsigned()->index();

            $table->foreign('institute_id')->references('id')->on('institute');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_id');
        Schema::dropIfExists('admin_id');

        Schema::dropIfExists('vehicle_type');
    }
}
