<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingUsernameUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username');

        });
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('institute_id')->unsigned()->index();
            $table->foreign('institute_id')->references('id')->on('institute');
            $table->bigInteger('user_type_id')->unsigned()->index();
            $table->foreign('user_type_id')->references('id')->on('user_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_id');
        Schema::dropIfExists('user_type_id');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('username');
        });

    }
}
